# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ 
      ./hardware-configuration.nix
      ./modules/packages.nix
      ./modules/fonts.nix
      ./modules/sh.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda"; 

  networking.hostName = "tsukasa"; 
  networking.networkmanager.enable = true;  

  time.timeZone = "Asia/Kolkata";

  i18n.defaultLocale = "en_US.UTF-8";
   console = {
     font = "Lat2-Terminus16";
     useXkbConfig = true;
   };

  zramSwap = {
    enable = true;
    algorithm = "lz4";
   }; 
  boot.kernelPackages = pkgs.linuxPackages_latest;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };
  hardware.opengl.enable = true;

  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
   };

  programs.zsh.enable = true;
  users.defaultUserShell = pkgs.zsh;
  services.openssh.enable = true;

  system.copySystemConfiguration = true;

  system.stateVersion = "22.05"; # Did you read the comment?

}

