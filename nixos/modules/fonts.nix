{ config, pkgs, ... }:
 
{
 fonts.fonts = with pkgs; [
  (nerdfonts.override {fonts = ["Overpass"];})
  noto-fonts
  noto-fonts-cjk
  noto-fonts-emoji
  liberation_ttf
  overpass
  ];

  fonts.fontconfig.defaultFonts.monospace = ["OverpassMono Nerd Font"];
  fonts.fontconfig = {
      enable = true;
      #dpi = 100;
      antialias = true;
      hinting = {
        autohint = true;
        enable = true;
        style = "hintfull";
      };
      subpixel.lcdfilter = "default";
    };
}
