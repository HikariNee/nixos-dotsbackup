{ config, pkgs, ... }:

{
   users.users.hikari = {
     isNormalUser = true;
     extraGroups = [ "wheel" ];
     packages = with pkgs; [
       firefox-esr-wayland
       foot
     ];
     initialPassword = "hespilibrem";
   };

  environment.systemPackages = with pkgs; [
     vim_configurable
     wget
     lynx
     sway
     swaybg
     swaylock-effects
     autotiling
     swaynotificationcenter
     termusic
     wofi
     git
     wpgtk
     waybar
     mpv
     yt-dlp
     bat
     ripgrep
     exa
     fd
     neofetch
     w3m
     imagemagick
     sirula
     ranger
     python3
     wlogout 
     wl-clipboard
     libsixel
     swappy
     cava 
     slurp
     grim 
   ];
	
}
