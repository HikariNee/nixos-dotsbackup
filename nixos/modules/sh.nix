{ config, pkgs, ... }:

{
programs.zsh = {
  shellAliases = {
	nixos-update = "sudo nixos-rebuild switch";
	edit-nix = "sudo vim /etc/nixos/configuration.nix";
        edit-sh = "sudo vim /etc/nixos/modules/sh.nix";
        edit-font = "sudo vim /etc/nixos/modules/fonts.nix";
        edit-package = "sudo vim /etc/nixos/modules/packages.nix";
        ls = "exa";
        grep = "rg";
        cat = "bat";
        find = "fd";
 };
 enableCompletion = true;
 autosuggestions.enable = true;
 autosuggestions.async = true;
 syntaxHighlighting.enable = true;
 };
}

